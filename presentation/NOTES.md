
Pre-flight Checklist
----------------------------------------------------------------

* Turn off speakers on laptop

* Open browser on presentation

* F11 [full-screen]

* Share screen

* PgDn (and PgUp) to advance slides


Script
----------------------------------------------------------------

#### Title

Hi, we're Ken and devrandom!

The Lightning Signer Project is an open-source project, please feel
free to contact us on keybase or twitter

BTW - if something is unclear or there are questions please feel free
to interrupt me and we can address immediately.

segue: we're concerned that the world is used to securing bitcoin wallets
and lightning nodes are much more difficult to secure ...

#### Hot Wallets

Bitcoin nodes do not need to have funds hot to perform their functions.

A business can easily keep the majority of it's bitcoin funds in
offline wallets.

This is not the case for lightning; the channels must have funds online
with keys available for lightning protocol operations.

The amount of money exposed in a large-retailer's lightning
infrastructure could easily be huge.  If a Target or Walmart had ten
million customer channels, each with an average balance of $100 we'd have
1 Billion dollars on a connected server with hot keys.

It's critical to use best practices for these servers.

Best practices for hot wallets include use of Secure Elements and
Multi-Signature transactions.

With lightning, you can't use standard bitcoin multisig because the
BOLT-specified transaction formats do not include multisig.

seque: What sort of attacks can we expect?

#### Compromised Node Attacks

The most direct attack is where the bad guy can compromise the
lightning node itself.

In this case they can perform layer 2 attacks like simply sending
payments to their own node elsewhere in the lightning network.

Or they can close the channels and send the funds to their own layer 1
address.

If they've compromised the node then they can also steal any funds in
the on-chain wallet as well

segue: But even if they can't compromise the node entirely there are
still many other attacks ...

#### Compromised Secrets + Evil Counterparty Attacks

If an attacker can learn the channel secrets AKA "hot keys" for your
node and can compromise a channel counterparty there are a lot of ways
they can steal your funds.

It's important to note, they don't need to compromise a counterparty
if they can **be** the counterparty; these attacks are serious for
nodes that promiscuously accept inbound channels (like large
retailers!)

These examples were well documented in the "Lightning 101 For
Exchanges" blog post by Chris Stewart from Suredbits.

Some of these attacks are direct and immediate.  Others involve a race
for the funds where the attacker will likely try and hamper your
ability to respond with a synchronized DoS of some sort.

segue: Some attacks don't even require a compromise or leaked secrets ...

#### Forwarding Exploit: Evil Counterparty + Eclipse

If an attacker can hide the current blockchain state from you (AKA an
"Eclipse Attack") then they can get you to forward a payment to a node
they control and then when it is your turn to claim the upstream
payment you discover the channel has been closed.

#### State Misstep: Evil Counterparty + Hack

Another attack is where the attacker can confuse you about the state
of a channel and get you to sign a transaction that you previously
revoked (meaning you already released the revocation secret) they can
then claim all of the channel funds with a justice transaction.

A good way to induce a state misstep is to cause your node to have to
restore from a slightly stale state backup of some sort.

segue: Can we defend against attacks like these?

#### Our Goal

[I think we let the slide do the talking and just sort pause for second?]

segue: How can we achieve this?

#### Our Approach

The straightforward thing to do is remove the hot keys and secrets
from the lightning node itself and sequester them in an external
signer.

We defined an RPC API using protobuf and grpc so the lightning node
can submit requests to the external signer.

When the node needs signatures it submits requests to the signer and
the signer returns the needed signatures.

The is exactly analogous to how hardware wallets work today with
on-chain bitcoin wallets; the difference is that the lightning
protocol is significantly more stateful and complicated.

segue: What security benefits do we gain from this approach?

#### External Signer Security Features

There are some pretty basic security advantages to external signing.

First, the external signer has a much smaller attack surface in
general then a lightning node:

  - The external signer doesn't have connections to each of the channel counterparties
  
  - The external signer doesn't participate in gossip with even more nodes
  
  - The external signer doesn't need to know about the graph
  
  - In general, lightning nodes are fairly complex and have significant feature
    development velocity.  This is wonderful, but it is also risky ...
    
Second, the actual signing operations in the external signer are
performed on independently composed transactions.  This means that
instead of the lightning node saying "here is my new remote commitment
transaction, please sign it", instead the lightning node says "I want
a new remote commitment with X satoshis to_local, Y satoshis to_remote
and the following HTLC specs".  This is much more secure because it
removes the abiity of an attacker to mutate the transaction in a
subtle way for example using a transaction version that has new
semantics.

Next, the external signer can enforce policies for each request.
These policies can be "tuned" for the context that the node is signing
in. More on that after the next slide ...

segue: Finally, the external signer can utilize security enhanced hardware

#### Using a Secure Element

The term "secure element" is very general.  Security enhanced hardware
comes in a lot of different forms, with different available resources.

In the simplest case, the majority of the external signer's code can
reside in the secure element.  Even in this case the secure element
should not be directly connected to the network.

In the case where the secure element has limited resources the
external signing can still be securely performed.  Basically the
secure element remembers the hash of a merkleized state tree after the
last request.  When the next request is made any needed state can be
provided externally as a merkle proof which the secure element can
easily validate.  In the response the secure element also returns the
updated branches of the merkle state tree back to the front end.

#### More about Policies

Policies can be enforced for defined contexts:

  - A "merchant context" node can have a policy that it only receives payments.
  
  - If a merchant node ever sends payments, they can be restricted to
    a configured whitelist of destinations.
    
  - A node in a "routing context" can enforce a policy that outgoing
    HTLCs are only signed if there is a correct matching incoming
    HTLC.
    
  - A "consumer context" node can enforce velocity controls on
    outgoing payments.  Any attempt to send too much at once is
    immediately denied.

#### Multi-Party Signing

A significant improvement in security can be gained by using multiple
independent external signers working together to sign the transactions.

In this configuration, an attacker who manages to compromise a
minority subset of the external signers cannot steal the funds; each
of the signers only has a portion of the private key material.

Schnorr signatures make this implementation much more feasible to implement.

#### Dependencies

The external signers need some additional support to validate
lightning transactions.

The UTXO-Set-Oracles in this diagram are servers which watch the
bitcoin blockchain, compute a tree describing all of the UTXOs and
sign the root of this tree.  Using these signed root attestations,
proofs of UTXO inclusion and exclusion can be presented to the
signer's secure element and easily validated.

Put differently, the signer's secure element needs to know that the
funding UTXO is still in the blockchain for many of the requests it
signs to be valid.

We also need proofs of exclusion because the secure element needs to
know that the commitment transaction has **not** been published to the
blockchain in order to sign other requests.

These UTXO-Set-Oracle servers do not currently exist, but we don't
think they are too hard to implement. They would likely be useful for
many other applications as well.

To prevent eclipse attacks, we'll also need to provide the secure element
with a proof of the real time which helps it to check that the proofs
of inclusion or exclusion are not stale.  This can be done using the
Roughtime protocol, which is already implemented and operating.

#### Current State

We have a functioning PoC with a fork of c-lightning.

We replaced c-lightning's signing daemon with a proxy which forwards
requests to the external signer.

The PoC passes the core c-lightning integration suite using the
external signer

We've also integrated with LDK (rust-lightning).  We've created a reference
lightning node implementation named "lightning-rod" using the LDK
which targets enterprise requirements (the folks who will need eternal
signing most)

#### Looking Forward

Some of the future protocol improvements in the bitcoin and lightning
protocols effect the lightning signer project.

Eltoo makes things better in a couple ways:

  - First, it removes the O(N) storage requirement of revoked
    payment_hash values which are needed for justice transactions.
    
  - It also removes several attack vectors which utilize justice
    transactions.
    
Schnorr signatures makes the threshold signature implementation needed
for the Multi-Party Signing much easier.

#### Project Roadmap

The first step is to implement the external signer as a standalone
rust program using the LDK.  This is where we are now.

We currently integrate with c-lightning and rust-lightning.  We pass
all relevent core c-lightning integration tests using the external
signer.

We've spent some time integrating with lnd, but it's more difficult
because lnd's internal signing structure abstracts away the context we
need to perform the signing. This effort will require more work in the
future.

We've also begun to implement our own reference LDK-based lightning
node called "lightning rod" with an eye towards enterprise
requirements where external signing will be most needed

The next stage in the milestone is to incorporate secure elements.
There may be more than one version of this; secure elements quite
diverse and one size might not fit all here.

We're interested in finding partners who require secure elements and
other partners who provide secure elements to help guide this part of
the implementation.

Finally we'll implement a multi-party version of the external signer
to offer still greater security.

