### [Back to Lightning-Signer Home](../README.md)

These diagrams show the remote signing API calls in the context of the
[BOLT #2](https://github.com/lightningnetwork/lightning-rfc/blob/master/02-peer-protocol.md)
specified protocol.

<br>

- [Channel Establishment Sequence](./channel-establishment.md)
- [Normal Operation Sequence](./normal-operation.md)
- [Dual Funding Sequence](./v2-channel-establishment.md)
- [Splicing Sequence](./splicing.md)
